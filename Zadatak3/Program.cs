﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak3
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + @"\data.csv"; 
            Dataset dataset1 = new Dataset(path);
            User user1 = User.GenerateUser("Matija");
            ProtectionProxyDataset protectionProxy1 = new ProtectionProxyDataset(user1);
            VirtualProxyDataset virtualProxy1 = new VirtualProxyDataset(path);
            DataConsolePrinter Printer = new DataConsolePrinter();
            Printer.Print(protectionProxy1);
            Printer.Print(virtualProxy1);
        }
    }
}