﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak5
{
    class Program
    {
        static void Main(string[] args)
        {
            //Z5 
            Console.WriteLine("Zadatak 5:");
            ITheme hackerTheme = new HackerTheme();
            ReminderNote reminderNote = new ReminderNote("Fake hacking text...", hackerTheme);
            reminderNote.Show();
            //Z6
            Console.WriteLine("Zadatak 6:");
            ITheme lightTheme = new LightTheme();
            GroupNote groupNote1 = new GroupNote("Organiziranje zabave", lightTheme);
            groupNote1.addPerson("Nikola Peric");
            groupNote1.addPerson("Petar Peric");
            groupNote1.Show();
            GroupNote groupNote2 = new GroupNote("Hackaton", hackerTheme);
            groupNote2.addPerson("Josip");
            groupNote2.addPerson("Marko");
            groupNote2.addPerson("Marija");
            groupNote2.removePerson("Marko");
            groupNote2.Show();
            //Z7
            Console.WriteLine("Zadatak 7: ");
            Notebook notebook = new Notebook();
            notebook.AddNote(reminderNote);
            notebook.AddNote(groupNote1);
            notebook.AddNote(groupNote2);
            notebook.Display();
            Console.WriteLine("Zajednicka tema: ");
            Notebook sameThemeNotebook = new Notebook(lightTheme);
            sameThemeNotebook.AddNote(reminderNote);
            sameThemeNotebook.AddNote(groupNote1);
            sameThemeNotebook.AddNote(groupNote2);
            sameThemeNotebook.Display();
        }
    }
}
