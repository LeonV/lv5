﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak5
{
    class GroupNote : Note
    {
        private List<string> names;
        public GroupNote(string message, ITheme theme) : base(message, theme) { names = new List<string>(); }
        public override void Show()
        {
            this.ChangeColor();
            Console.WriteLine("REMINDER: ");
            foreach (string person in names) {
                Console.WriteLine(person);
            }
            string framedMessage = this.GetFramedMessage();
            Console.WriteLine(framedMessage);
            Console.ResetColor();
        }
        public void addPerson(string name) 
        {
            names.Add(name);
        }
        public void removePerson(string name)
        {
            names.Remove(name);
        }
    }
}
