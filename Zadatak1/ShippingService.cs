﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak1
{
    class ShippingService
    {
        private double pricePerKG;
        public ShippingService(double PricePerKilogram) { pricePerKG = PricePerKilogram; }
        public double PricePerKG { get; set; }
        public double CalculatePrice(double Weight){
            return Weight / pricePerKG;
        }
    }
}
