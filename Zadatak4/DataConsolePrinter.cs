﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak4
{
    class DataConsolePrinter
    {
        public DataConsolePrinter() { }
        public void Print(IDataset dataset) 
        {
            IReadOnlyCollection<List<string>> Data = dataset.GetData();
            if (Data == null)
                Console.WriteLine("Unable to print");
            else
                foreach (List<string> lines in Data) 
                {
                    foreach (string item in lines)
                    {
                        Console.Write(item + " ");
                    }
                    Console.WriteLine();
                }
        }
    }
}
